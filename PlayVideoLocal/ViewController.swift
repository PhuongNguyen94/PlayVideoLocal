//
//  ViewController.swift
//  PlayVideoLocal
//
//  Created by Administrator on 1/25/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    @IBOutlet weak var videoViewContainer: UIView!
    var player: AVPlayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initVideoLayer()
        // Do any additional setup after loading the view, typically from a nib.
    }


    func initVideoLayer() {
        
        //MARK : - Get the path string
        let videoString:String? = Bundle.main.path(forResource: "6SHU", ofType: "mp4")
        guard let unwrappedVideoPath = videoString else {return}
        
        //MARK: -  Convert the path string to a url
        let videoUrl = URL(fileURLWithPath: unwrappedVideoPath)
        
        self.player = AVPlayer(url: videoUrl)
        
        //MARK: - reate a video layer
        let layer: AVPlayerLayer = AVPlayerLayer(player: player)
        
       
        layer.frame = videoViewContainer.bounds
       
        layer.videoGravity = AVLayerVideoGravity.resizeAspectFill
     
        videoViewContainer.layer.addSublayer(layer)
    }


    @IBAction func PlayClick(_ sender: Any) {
        
        player?.play()
    }
}

